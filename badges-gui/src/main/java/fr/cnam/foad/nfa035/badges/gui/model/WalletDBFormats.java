package fr.cnam.foad.nfa035.badges.gui.model;

/**
 * Classe énumération des cas possibles d'accès au DAO
 */
public enum WalletDBFormats {

    jsonBadge,
    directAccess,
    simple;

}
